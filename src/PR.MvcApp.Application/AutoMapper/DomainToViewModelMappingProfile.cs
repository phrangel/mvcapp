﻿using AutoMapper;
using PR.MvcApp.Application.ViewModels;
using PR.MvcApp.Domain.Entities;

namespace PR.MvcApp.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Cliente, ClienteViewModel>();
            CreateMap<Cliente, ClienteEnderecoViewModel>();
            CreateMap<Endereco, EnderecoViewModel>();
            CreateMap<Endereco, ClienteEnderecoViewModel>();

        }
    }
}
