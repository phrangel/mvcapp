﻿using AutoMapper;
using PR.MvcApp.Application.ViewModels;
using PR.MvcApp.Domain.Entities;

namespace PR.MvcApp.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ClienteViewModel, Cliente>();
            CreateMap<ClienteEnderecoViewModel, Cliente>();
            CreateMap<EnderecoViewModel, Endereco>();
            CreateMap<ClienteEnderecoViewModel, Endereco>();

        }
    }
}
