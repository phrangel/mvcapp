﻿using PR.MvcApp.Application.ViewModels;
using System;
using System.Collections.Generic;

namespace PR.MvcApp.Application.Interfaces
{
    public interface IClienteAppService : IDisposable
    {
        ClienteEnderecoViewModel Adicionar(ClienteEnderecoViewModel clienteEnderecoViewModel);

        ClienteViewModel ObterPorID(Guid id);

        IEnumerable<ClienteViewModel> ObterTodos();

        ClienteViewModel ObterPorCpf(string cpf);

        ClienteViewModel Atualizar(ClienteViewModel clienteViewModel);

        void Remover(Guid id);
    }
}
