﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PR.MvcApp.Infra.CrossCutting.MvcFilters
{
    public class GlobalErrorHandler : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //LOG Auditoria

            if (filterContext.Exception != null)
            {
                //Manipular a EX
            }
            base.OnActionExecuted(filterContext);
        }
    }
}
