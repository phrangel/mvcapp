﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PR.MvcApp.UI.Sistema.Startup))]
namespace PR.MvcApp.UI.Sistema
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
