﻿using Dapper;
using PR.MvcApp.Domain.Entities;
using PR.MvcApp.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PR.MvcApp.Infra.Data.Repository
{
    public class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public IEnumerable<Cliente> ObterAtivos()
        {
            return Buscar(c => c.Ativo);
        }

        public Cliente ObterPorCpf(string cpf)
        {
            return Buscar(c => c.CPF == cpf).FirstOrDefault();
        }

        public Cliente ObterPorEmail(string email)
        {
            return Buscar(c => c.Email == email).FirstOrDefault();
        }

        public override void Remover(Guid id)
        {
            var cliente = ObterPorId(id);
            cliente.Ativo = false;
            Atualizar(cliente);
        }

        public override IEnumerable<Cliente> ObterTodos()
        {
            var cn = Db.Database.Connection;

            var sql = "SELECT * FROM Clientes";

            return cn.Query<Cliente>(sql);
        }

        public override Cliente ObterPorId(Guid id)
        {
            var cn = Db.Database.Connection;

            var sql = @"SELECT * FROM Clientes c " +
                      "LEFT JOIN Enderecos e " +
                      "ON c.ClienteId = e.ClienteId " +
                      "WHERE c.ClienteId = @sid ";

            var cliente = cn.Query<Cliente, Endereco, Cliente>(sql,
                (c, e) =>
                {
                    c.Enderecos.Add(e);

                    return c;
                }, new { sid = id }, splitOn: "ClienteId, EnderecoId");


            return cliente.FirstOrDefault();


        }
    }
}
